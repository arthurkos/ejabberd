#!/bin/bash

##################################################
#Script ejabberd Arthur Kenny Virgil Khaireddine #
##################################################

clear 

# Creation du menu

menu=0
while [ "$menu" -eq 0 ]; do

	echo "###################################"
	echo ""
	echo "		MENU			"
        echo ""
	echo "	1 - Installation "
        echo "	2 - Ajout / Suppresion utilisateur "
        echo "	3 - Désinstallation"
        echo "	4 - Backup / Sauvegarde"
        echo "	5 - Sortir"
	echo ""
       
	 # prise du choix de l'utilisateur
	echo "###############################"			        
	read -p "##  Quel est votre choix ? " choix 
	echo "###############################"


case "$choix" in
        # Installation de ejabberd
1)
	echo ""
        echo -e " 		Début de l'installation de Ejabberd \n"
	echo ""
        wget https://www.process-one.net/downloads/downloads-action.php?file=/ejabberd/19.09.1/ejabberd_19.09.1-0_amd64.deb
        dpkg -i downloads-action.php\?file\=%2Fejabberd%2F19.09.1%2Fejabberd_19.09.1-0_amd64.deb
        echo "		Démarrage de Ejabebrd"
        /opt/ejabberd-19.09.1/bin/start
	/opt/ejabberd-19.09.1/bin/status
	domaine=`hostname -f | cut -d. -f2`
	echo " admin@"$domaine" | mot de passe: admin"
	sed -i -r 's/.*ip_access: trusted_network*./ip_access: all/g' /opt/ejabberd/conf/ejabberd.yml
	
	echo " "
	echo "    		Fin de l'instalation de Ejabberd "
	echo " "
	read -p "    Press Enter to continue"
	menu=0

;;
# cas ajout utilisateur
2)
	echo ""
        echo -e "	1 - Ajouter un utilisateur \n"
        echo ""
	echo -e "	2 - Supprimer un utilisateur \n"
        echo ""
	echo "#########################################"
	read -p "# Entrez un chiffre pour votre choix ? " choix2
	echo "#########################################"
                
		case "$choix2" in
                      
			1)
			echo ""
			read -p " - Entrez votre nom utilisateur: " user
			echo ""
			domaine=`hostname -f | cut -d. -f2`
			echo ""
			read -p " - Entrez votre mot de passe: " mdp
			/opt/ejabberd-19.09.1/bin/ejabberdctl register "$user" "$domaine" "$mdp"
			echo ""
			read -p "    Press Enter to continue"
			;;

			2)
			echo ""
			read -p " - Entrez votre nom utilisateur: " user
			echo ""
			domaine=`hostname -f | cut -d. -f2`
			/opt/ejabberd-19.09.1/bin/ejabberdctl unregister "$user" "$domaine"
                        echo ""
			echo " Votre utilisateur vient d'être supprimé !"
			echo ""
			read -p "    Press Enter to continue"
			
			;;
                
	esac
	menu=0
	
;;
# cas desinstallaton
3)
	echo ""
	echo "		Début de la désinstalation de Ejabberd "
	echo ""
	/opt/ejabberd-19.09.1/bin/preuninstall.sh
        apt purge -y ejabberd
        rm -Rf /opt/ejabberd*
	rm -Rf downloads-action.php?file=%2Fejabberd%2F19.09.1%0_amd64.deb
	echo ""
	echo "		Fin de la désinstalation de Ejabberd "
	echo ""
	read -p "  Press Enter to continue "
        menu=0
;;
#cas backup
4)
        read -p " chemin du backup " chemin
	/opt/ejabberd-19.09.1/bin/ejabberdctl backup "$chemin".dump
	menu=0
;;
# sortie du script
5)
	echo ""
	echo "	#############"
	echo "	#           #"
        echo "	#  BYE-BYE  #"
	echo "	#	    #"
	echo "	#############"
	echo ""        
exit
;;
# tout autre cas on retourne au script
*)
	echo ""
	echo "	##################"
        echo "	# Choix Invalide #"
	echo "	##################"
	echo ""
	menu=0
esac
done
